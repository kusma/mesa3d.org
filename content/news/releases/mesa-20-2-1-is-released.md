---
title:    "Mesa 20.2.1 is released"
date:     2020-10-14 10:44:26
category: releases
tags:     []
---
[Mesa 20.2.1](https://docs.mesa3d.org/relnotes/20.2.1.html) is released. This is a bug fix release.

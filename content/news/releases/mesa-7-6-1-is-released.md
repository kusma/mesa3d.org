---
title:    "Mesa 7.6.1 is released"
date:     2009-12-21 00:00:00
category: releases
tags:     []
summary:  "[Mesa 7.6.1](https://docs.mesa3d.org/relnotes/7.6.1.html) is released. This is a bug-fix
release fixing issues found in the 7.6 release."
---
[Mesa 7.6.1](https://docs.mesa3d.org/relnotes/7.6.1.html) is released. This is a bug-fix
release fixing issues found in the 7.6 release.

Also, [Mesa 7.7](https://docs.mesa3d.org/relnotes/7.7.html) is released. This is a new
development release.
